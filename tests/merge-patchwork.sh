#!/bin/bash
set -eu -o pipefail

export baserepo="git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git"
export patchwork="https://patchwork.kernel.org/patch/7248581/ https://patchwork.kernel.org/patch/7251061/"
export ref="v4.19"
export WORKDIR=$(mktemp -d)

pushd $BASEDIR
    ansible-playbook -i hosts merge.yml

    # Check for strings in rc_merge
    cat rc_merge

    # Check for environment-variables.txt
    stat environment-variables.txt
    grep "^patchwork=" environment-variables.txt
    grep "^patchwork_session_cookie=" environment-variables.txt

popd
