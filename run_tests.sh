#!/bin/bash
set -eu -o pipefail

echo "Running tests"

export BASEDIR="$( cd "$(dirname "$0")" ; pwd -P )"

./tests/merge-patchwork.sh
